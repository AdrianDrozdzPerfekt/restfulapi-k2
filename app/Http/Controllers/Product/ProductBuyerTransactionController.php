<?php

namespace App\Http\Controllers\Product;

use App\Product;
use App\Buyer;
use App\User;
use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\DB;

class ProductBuyerTransactionController extends ApiController
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product, User $buyer)
    {
        $rules = [
            'quantity' => 'required|integer|min:1'
        ];

        $this->validate($request, $rules);

        if ($buyer->id == $product->seller_id) {
            return $this->errorResponse(
                'The buyer must be diffrent than the seller',
                409
            );
        }

        if (!$buyer->isVerified()) {
            return $this->errorResponse(
                'Only verified users can make a transaction',
                422
            );
        }

        if (!$product->seller->isVerified()) {
            return $this->errorResponse(
                'Only verified sellers can sell products',
                422
            );
        }

        if (!$product->isAvailable()) {
            return $this->errorResponse(
                'Product is not available',
                404
            );
        }

        if ($product->quantity < $request->quantity) {
            return $this->errorResponse(
                'Product does not have enough units',
                409
            );
        }

        return DB::transaction(function() use ($request, $product, $buyer) {

            $product->quantity -= $request->quantity;
            $product->save();

            $transaction = Transaction::create([
                'quantity' => $request->quantity,
                'buyer_id' => $buyer->id,
                'product_id' => $product->id,
            ]);

            return $this->showOne($transaction, 201);
        });


    }
}
