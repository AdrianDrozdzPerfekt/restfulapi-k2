<?php

namespace App\Http\Controllers\Buyer;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Buyer;

class BuyerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Buyer::all() -> Zwroci de facto wszystkich uzytkownikow
        $buyers = Buyer::has('transactions')->get();    //pobierz wszystich, 
                                                        //dla ktorych wystepuje
                                                        //relacja `transactions`
        return $this->showAll($buyers);
    }

    /**
     * Display the specified resource.
     *
     * @param  Buyer  $buyer
     * @return \Illuminate\Http\Response
     */
    public function show(Buyer $buyer)
    {
        return $this->showOne($buyer);   
    }
}
