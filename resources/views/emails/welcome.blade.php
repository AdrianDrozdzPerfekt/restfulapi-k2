Hello {{$user->name}},

Thank you for registering at out site.
To verify your account please use this link:
{{route('verify', ['token'=>$user->verification_token])}}