@component('mail::message')
# Hello {{$user->name}},

To verify your new e-mail please use this link:

@component('mail::button', ['url' => {{route('verify', ['token'=>$user->verification_token])}}])
Verify Email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent