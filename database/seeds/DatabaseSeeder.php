<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Category;
use App\Product;
use App\Transaction;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //wyłaczanie spr kluczy obcych w bazie
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        //czyszczenie tabel
        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        DB::table('category_product')->truncate();

        //usuanie sluchanie na eventach aby nie wysylac maili
        User::flushEventListeners();
        Category::flushEventListeners();
        Product::flushEventListeners();
        Transaction::flushEventListeners();

        $usersQuantity = 1000;
        $categoriesQuantity = 30;
        $productsQuantity = 1000;
        $transactionsQuantity = 1000;

        factory(User::class, $usersQuantity)->create();
        factory(Category::class, $categoriesQuantity)->create();

        factory(Product::class, $productsQuantity)->create()->each(function($product) { //dla każdego produktu
        	//wylosuj randomowo 1-5 id kategorii
        	$categories = Category::all()->random(mt_rand(1, 5))->pluck('id');

        	//przypisz cateorie do produktu
        	$product->categories()->attach($categories);

        });

        factory(Transaction::class, $transactionsQuantity)->create();
    }
}
